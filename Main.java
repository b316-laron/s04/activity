package com.zuitt;
import java.util.Date;

public class Main {

    public Main(){
    }

    public static void main(String[] args) {
        User user1 = new User("Terrence Gaffud", 25, "terrencegaffud@gmail.com", "Quezon City");
        user1.call();




        Date startDate = new Date(051423); // Replace with your desired start date in milliseconds
        Date endDate = new Date(061423);

        Course course1 = new Course("M12345", "Introduction to Java", 14, 81000, startDate, endDate, user1);
        course1.call();

    }
    
    // Hi! I'm {user}. I'm {age} years old. You can reach me via my email: {email}. When I'm off work, I can be found at my house in {address}.

    // Welcome to the course {course code}. This course can be described as an {course description}. Your instructor for this course is Sir/Ma'am {user's name}. Enjoy!

    //USER
    // Name - String
    // Age - Int
    // Email - String
    // Address - String

    //COURSE
    // Name - String
    // Description - String
    // Seats - Int
    // Fee - double
    // StartDate - Date
    // EndDate - Date
    // Instructor - User
}
